let http = require('http');

const hostname = "localhost";
const port = 4000;


let server = http.createServer((req, res) => {


    if(req.url === "/" && req.method === "GET"){
        
        res.writeHead(200,{'Content-Type': 'text/plain'});
        res.end("Welcome to booking system!");

    }

    else if(req.url === "/profile" && req.method === "GET"){
        
        res.writeHead(200,{'Content-Type': 'text/plain'});
        res.end("Welcome to your profile.");

    }

    else if(req.url === "/courses" && req.method === "GET"){
        
        res.writeHead(200,{'Content-Type': 'text/plain'});
        res.end("Here's our courses available");

    }

    
    else if(req.url === "/addCourse" && req.method === "POST"){
        
        res.writeHead(200,{'Content-Type': 'text/plain'});
        res.end("Add course to our resources");

    }

    else if(req.url === "/updateCourse" && req.method === "PUT"){
        
        res.writeHead(200,{'Content-Type': 'text/plain'});
        res.end("Update a course to our resources");

    }

    else if(req.url === "/archiveCourse" && req.method === "DELETE"){
        
        res.writeHead(200,{'Content-Type': 'text/plain'});
        res.end("Archive a course to our resources");

    }




});

server.listen(port, hostname, () => {

    console.log(`Server running at http://${hostname}:${port}`);
});